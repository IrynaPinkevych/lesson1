//1. function getType(param) {} => return type of param
let getType = function (param){
    return typeof param;
}
//console.log(getType(5));
//console.log(getType('test'));
//console.log(getType(true));

//2. function getWholeString(one, two, three) {} => return concatenated string (onetwothree)
let getWholeString = function (one, two, three){
    return String(one)+String(two)+String(three);
}
//console.log(getWholeString('a','b','c'));
//console.log(getWholeString(5,'b','c'));
//console.log(getWholeString(5,6,45));

//3.function getResult() {} => Math.random() - >0.5 (URA), <0.5 (NE URA)
let getResult = function (){
    return Math.random()>0.5 ? 'URA!!!' : 'neURA(((';
}
//console.log(getResult());
//console.log(getResult());
//console.log(getResult());

//4. function getObject(['test', 'test1']) {} => Object with properties from [] ({test: 0, test1: 0})
let getObject = function (arr){
    let obj = {};
    arr.forEach(elem => obj[elem] = 0);
    return obj;
}
//console.log(getObject(['test', 'test1']));
//console.log(getObject([1,2,3,4,5]));

//5.** function countFactorial(n) {} => ввожу что хочу =) получаю результат
let countFactorial = function(n){
    return (n>=1) ? n*=countFactorial(n-1) : 1;
}
//console.log(countFactorial(4));
//console.log(countFactorial(5));
//console.log(countFactorial(+prompt('Введите число')));